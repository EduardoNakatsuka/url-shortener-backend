import { nanoid } from "nanoid";
import { Link, LinkFields } from "../models/Link";

export class LinkRepository {
  link: Link;

  constructor () {
    this.link = new Link();
  };

  createLink = (fullUrl: string): Promise<LinkFields> => {
    this.validateUrl(fullUrl);

    return this.link.create({
      fullUrl,
      shortUrl: nanoid(Link.NANOIDSIZE)
    });
  };

  getTop100 = (): Promise<LinkFields[]> => {
    return this.link.getTop100();
  };

  getLink = async (shortUrl: string): Promise<LinkFields> => {
    return this.link.increment(shortUrl);
  };

  validateUrl = (url: string): void => {
    // https://nodejs.org/api/url.html#url_new_url_input_base
    try {
      new URL(url);
    } catch (error) {
      throw new Error('Invalid URL');
    }
  };
}