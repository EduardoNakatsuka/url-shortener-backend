
exports.up = function(knex) {
  return knex.schema.createTable('links', table => {
    table.increments('id').primary();
    table.integer('views').unsigned().defaultTo(0);
    table.string('full_url').unique().notNullable();
    table.string('short_url', 10).index().unique().notNullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('links');
};
