import knex from 'knex';
import dotenv from 'dotenv';
dotenv.config();

export const db = knex({
  client: 'mysql2',
  connection: {
    host     : process.env.DB_HOST,
    port     : parseInt(process.env.DB_PORT || '3306'), // Typescript + knex issue :c
    user     : process.env.DB_USER,
    password : process.env.DB_PASSWORD,
    database : process.env.DB_DATABASE
  }
});
