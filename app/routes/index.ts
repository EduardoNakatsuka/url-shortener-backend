import { Router } from 'express';
import LinkController from '../controllers/LinkController';

const router = Router();

router.get('/', LinkController.index);
router.get('/:id', LinkController.show);
router.post('/create', LinkController.store);

export default router;
