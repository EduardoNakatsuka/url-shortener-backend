import { Request, Response } from 'express';
import { LinkFields } from '../models/Link';
import { LinkRepository } from '../repositories/LinkRepository';

class LinkController {
  linkRepository: LinkRepository;

  constructor() {
    this.linkRepository = new LinkRepository();
  }

  index = async (request: Request, response: Response): Promise<Response<LinkFields[]>> => {
    try {
      const top100 = await this.linkRepository.getTop100();

      return response.status(200).json(top100);
    } catch (error) {
      return response.status(500).json(error);
    }
  };

  // Promise<Response<any, Record<string, any>>>
  store = async (request: Request, response: Response): Promise<Response<LinkFields>> => {
    const { fullUrl } = request.body;

    try {
      const link = await this.linkRepository.createLink(fullUrl);

      return response.status(201).json(link);
    } catch (error: any) {
      // TODO: Prod - treat this error, since it can return too much information for the user;
      return response.status(500).json({
        message: error.sqlMessage || error.message
      });
    }
  };

  show = async (request: Request, response: Response): Promise<void | Response<any, Record<string, any>>> => {
    try {
      const link = await this.linkRepository.getLink(request.params.id);

      return response.redirect(link.full_url);
    } catch (error) {
      return response.status(500).json(error);
    }
  };
}

export default new LinkController();