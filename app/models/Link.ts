import { db } from "../database/connection";

export type LinkFields = {
  id: number;
  views: number;
  full_url: string;
  short_url: string;
};

export type CreateLinkFields = {
  fullUrl: string;
  shortUrl: string;
};

export class Link {
  public static readonly NANOIDSIZE: number = 10; // https://zelark.github.io/nano-id-cc/
  // using same instance of table link is unsafe with knex;
  // table: Knex.QueryBuilder<LinkFields>;
  // constructor () {
  //   console.log('constructor called');
    
  //   this.table = db('links');
  // };

  create = async ({ fullUrl, shortUrl }: CreateLinkFields): Promise<LinkFields> => {
    const [id] = await db('links').insert({
      short_url: shortUrl,
      full_url: fullUrl,
    });

    return {
      id,
      views: 0,
      short_url: this.getShortUrl(shortUrl),
      full_url: fullUrl
    };
  };

  getShortUrl (hash: string): string {
    return new URL(hash, process.env.BASE_URL).href;
  };

  getTop100 (): Promise<LinkFields[]> {
    // DANGER!! GO TO README FOR MORE INFO!!
    return db('links').orderBy('views', 'desc').limit(100);
  };

  async increment (shortUrl: string): Promise<LinkFields> {
    // DANGER!! GO TO README FOR MORE INFO!!
    return await db('links').where({ short_url: shortUrl })
      .first()
      .increment('views', 1)
      .then(() => {
        return db('links').where({ short_url: shortUrl }).first();
      });
  }; 
}